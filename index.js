'use strict';
require('babel-core/register');

var fs = require('fs');

var stream = fs.createWriteStream('index.html');
stream.once('open', fd => stream.end('<!DOCTYPE html><html><body>Hello, world</body></html>'));
console.log('app started');
